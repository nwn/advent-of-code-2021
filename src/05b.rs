use std::io::{self, prelude::*};
use std::collections::HashMap;

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut map = HashMap::new();
    let mut incr_coord = |x, y| match map.entry((x, y)) {
        std::collections::hash_map::Entry::Vacant(vacant) => {
            vacant.insert(1);
        }
        std::collections::hash_map::Entry::Occupied(mut occupied) => {
            *occupied.get_mut() += 1;
        }
    };

    for line in lines {
        let (x1, y1, x2, y2) = {
            let mut coords = line.split(" -> ")
                .flat_map(|coord| coord.split(','))
                .map(|num| num.parse::<i32>().unwrap());
            let (x1, y1) = (coords.next().unwrap(), coords.next().unwrap());
            let (x2, y2) = (coords.next().unwrap(), coords.next().unwrap());
            (x1, y1, x2, y2)
        };
        let x_range: Vec<_> = if x1 <= x2 { (x1..=x2).collect() } else { (x2..=x1).rev().collect() };
        let y_range: Vec<_> = if y1 <= y2 { (y1..=y2).collect() } else { (y2..=y1).rev().collect() };
        if x1 == x2 {
            for y in y_range {
                incr_coord(x1, y);
            }
        } else if y1 == y2 {
            for x in x_range {
                incr_coord(x, y1);
            }
        } else {
            for (x, y) in x_range.into_iter().zip(y_range.into_iter()) {
                incr_coord(x, y);
            }
        }
    }

    let result = map.into_values()
        .filter(|&val| val > 1)
        .count();
    println!("{}", result);
}
