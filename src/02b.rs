use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut hor = 0;
    let mut dep = 0;
    let mut aim = 0;
    for line in lines {
        let mut words = line.split_whitespace();
        let cmd = words.next().unwrap();
        let amnt = words.next().unwrap().parse::<i32>().unwrap();

        match cmd {
            "forward" => {
                hor += amnt;
                dep += aim * amnt;
            }
            "down" => aim += amnt,
            "up" => aim -= amnt,
            _ => panic!("Invalid command: {}", cmd),
        }
    }
    println!("{}", hor * dep);
}
