use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);
    let vals = lines.map(|num| num.parse::<i32>().unwrap()).collect::<Vec<_>>();
    let sums = vals.windows(3).map(|window| window.iter().sum::<i32>()).collect::<Vec<_>>();
    let result = sums.windows(2).filter(|window| window[0] < window[1]).count();
    println!("{}", result);
}
