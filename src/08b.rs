use std::io::{self, prelude::*};
use std::collections::HashMap;

fn main() {
    let stdin = io::stdin();

    let mut sum = 0;
    let lines = stdin.lock().lines().map(Result::unwrap);
    for line in lines {
        fn str_to_bits(str: &str) -> u8 {
            let mut bits = 0;
            for byte in str.bytes() {
                bits |= 1 << (byte - b'a');
            }
            bits
        }

        let (mut inputs, outputs) = {
            let mut parts = line.split(" | ");
            let inputs: Vec<_> = parts.next().unwrap().split_whitespace().map(str_to_bits).collect();
            let outputs: Vec<_> = parts.next().unwrap().split_whitespace().map(str_to_bits).collect();
            (inputs, outputs)
        };

        inputs.sort_unstable_by(|lhs, rhs| lhs.count_ones().cmp(&rhs.count_ones()));
        let input_1 = inputs[0];
        let input_7 = inputs[1];
        let input_4 = inputs[2];
        let inputs_2_3_5 = &inputs[3..6];
        let inputs_0_6_9 = &inputs[6..9];
        let input_8 = inputs[9];

        fn is_superset_of(sup: u8, sub: u8) -> bool {
            (sup & sub) == sub
        }

        let mut digit_map = HashMap::new();
        digit_map.insert(input_1, 1);
        digit_map.insert(input_7, 7);
        digit_map.insert(input_4, 4);
        digit_map.insert(input_8, 8);

        for &input in inputs_0_6_9 {
            if is_superset_of(input, input_4) {
                digit_map.insert(input, 9);
            } else if is_superset_of(input, input_1) {
                digit_map.insert(input, 0);
            } else {
                digit_map.insert(input, 6);
            }
        }

        for &input in inputs_2_3_5 {
            if is_superset_of(input, input_1) {
                digit_map.insert(input, 3);
            } else if (input & input_4).count_ones() == 2 {
                digit_map.insert(input, 2);
            } else {
                digit_map.insert(input, 5);
            }
        }

        let mut num = 0;
        for output in outputs {
            num = 10 * num + digit_map[&output];
        }
        sum += num;
    }
    println!("{}", sum);
}

