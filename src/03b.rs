use std::io::{self, prelude::*};

const BIT_LENGTH: usize = 12;

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);
    let nums: Vec<_> = lines.map(|line| u32::from_str_radix(&line, 2).unwrap()).collect();

    let o2_rating = calculate_rating(nums.clone(), true);
    let co2_rating = calculate_rating(nums.clone(), false);
    println!("{}", o2_rating * co2_rating);
}

fn calculate_rating(mut nums: Vec<u32>, bit_criterion: bool) -> u32 {
    let mut mask = 0;
    for bit_idx in (0..BIT_LENGTH).rev() {
        let ones = nums.iter()
            .filter(|&num| (num >> bit_idx) & 1 != 0)
            .count();
        let bit = ((2 * ones >= nums.len()) == bit_criterion) as u32;
        mask |= bit << bit_idx;
        nums.retain(|&num| (num >> bit_idx) == (mask >> bit_idx));
        if nums.len() == 1 {
            break;
        }
    }
    nums[0]
}
