use std::io::{self, prelude::*};

const BIT_LENGTH: usize = 12;

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);
    let nums: Vec<_> = lines.map(|line| u32::from_str_radix(&line, 2).unwrap()).collect();

    let mut gamma = 0;
    for bit_idx in 0..BIT_LENGTH {
        let ones = nums.iter()
            .filter(|&num| (num >> bit_idx) & 1 != 0)
            .count();
        let bit = (2 * ones >= nums.len()) as u32;
        gamma |= bit << bit_idx;
    }
    let epsilon = gamma ^ ((1 << BIT_LENGTH) - 1);

    println!("{}", gamma * epsilon);
}
