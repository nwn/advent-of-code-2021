use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();

    let line = stdin.lock().lines().map(Result::unwrap).next().unwrap();
    let nums: Vec<_> = line.split(',').map(|num| num.parse::<i32>().unwrap()).collect();

    let min = *nums.iter().min().unwrap();
    let max = *nums.iter().max().unwrap();

    let min_fuel = (min..=max).map(|target| {
        nums.iter().map(|num| (num - target).abs()).sum::<i32>()
    }).min().unwrap();

    println!("{}", min_fuel);
}
