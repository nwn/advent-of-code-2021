use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();

    let lines = stdin.lock().lines().map(Result::unwrap);
    let known_patterns: usize = lines.map(|line| {
        let patterns = line.split(" | ").nth(1).unwrap().split_whitespace();
        patterns.filter(|pattern| [2, 3, 4, 7].contains(&pattern.len())).count()
    }).sum();

    println!("{}", known_patterns);
}
