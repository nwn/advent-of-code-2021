use std::io::{self, prelude::*};
use std::collections::HashSet;

fn main() {
    let stdin = io::stdin();

    let lines = stdin.lock().lines().map(Result::unwrap);
    let grid: Vec<Vec<_>> = lines.map(|line|
        line.bytes().map(|b| b - b'0').collect()
    ).collect();

    let mut basins = vec![];
    let mut visited = HashSet::new();
    for y in 0..grid.len() {
        for x in 0..grid[y].len() {
            if visited.contains(&(x, y)) || grid[y][x] == 9 { continue }
            let mut basin = 0;
            let mut to_visit = vec![(x, y)];
            while let Some((x, y)) = to_visit.pop() {
                if !visited.insert((x, y)) || grid[y][x] == 9 { continue }
                basin += 1;
                if x > 0 { to_visit.push((x - 1, y)); }
                if y > 0 { to_visit.push((x, y - 1)); }
                if x + 1 < grid[y].len() { to_visit.push((x + 1, y)); }
                if y + 1 < grid.len() { to_visit.push((x, y + 1)); }
            }
            basins.push(basin);
        }
    }
    basins.sort();
    let largest = &basins[(basins.len() - 3)..];

    println!("{}", largest[0] * largest[1] * largest[2]);
}
