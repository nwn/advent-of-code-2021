use std::io::{self, prelude::*};
use std::collections::HashSet;

fn main() {
    let stdin = io::stdin();

    let lines = stdin.lock().lines().map(Result::unwrap);
    let mut grid: Vec<Vec<_>> = lines.map(|line|
        line.bytes().map(|b| b - b'0').collect()
    ).collect();

    for step in 1.. {
        let mut to_incr = vec![];
        for y in 0..grid.len() {
            for x in 0..grid[y].len() {
                to_incr.push((x, y));
            }
        }
        let mut flashing = HashSet::new();
        while let Some((x, y)) = to_incr.pop() {
            grid[y][x] += 1; // May increment more than necessary, but bounded by neighbourhood.
            if grid[y][x] > 9 && flashing.insert((x, y)) {
                for y in neighbourhood(y, grid.len()) {
                    for x in neighbourhood(x, grid[y].len()) {
                        if !flashing.contains(&(x, y)) {
                            to_incr.push((x, y));
                        }
                    }
                }
            }
        }
        if flashing.len() == grid.len() * grid.len() {
            println!("{}", step);
            break
        }
        for (x, y) in flashing {
            grid[y][x] = 0;
        }
    }
}

fn neighbourhood(idx: usize, max: usize) -> impl Iterator<Item=usize> {
    let lower = idx.saturating_sub(1);
    let upper = idx.saturating_add(1).min(max.saturating_sub(1));
    lower..=upper
}
