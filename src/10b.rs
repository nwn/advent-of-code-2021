use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut scores = vec![];
    'lines: for line in lines {
        let mut stack = vec![];
        for ch in line.chars() {
            match ch {
                '(' | '[' | '{' | '<' => stack.push(ch),
                ')' => if stack.last() == Some(&'(') { stack.pop(); } else { continue 'lines },
                ']' => if stack.last() == Some(&'[') { stack.pop(); } else { continue 'lines },
                '}' => if stack.last() == Some(&'{') { stack.pop(); } else { continue 'lines },
                '>' => if stack.last() == Some(&'<') { stack.pop(); } else { continue 'lines },
                _ => panic!("Invalid char: {}", ch),
            }
        }
        let mut score = 0u64;
        while let Some(ch) = stack.pop() {
            score *= 5;
            score += match ch {
                '(' => 1,
                '[' => 2,
                '{' => 3,
                '<' => 4,
                _ => panic!("Invalid char: {}", ch),
            };
        }
        scores.push(score);
    }
    scores.sort();
    println!("{}", scores[scores.len() / 2]);
}
