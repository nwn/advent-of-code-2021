use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut result = 0;
    for line in lines {
        let mut stack = vec![];
        for ch in line.chars() {
            match ch {
                '(' | '[' | '{' | '<' => stack.push(ch),
                ')' => if stack.pop() != Some('(') { result += 3; break },
                ']' => if stack.pop() != Some('[') { result += 57; break },
                '}' => if stack.pop() != Some('{') { result += 1197; break },
                '>' => if stack.pop() != Some('<') { result += 25137; break },
                _ => panic!("Invalid char: {}", ch),
            }
        }
    }
    println!("{}", result);
}
