use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);

    let input_line = lines.next().unwrap();
    let mut inputs = input_line.split(',')
        .map(|num| num.parse::<i32>().unwrap());

    let mut boards = vec![];
    while lines.next().is_some() {
        let mut board = vec![];
        for _ in 0..5 {
            let line: Vec<_> = lines.next().unwrap()
                .split_whitespace()
                .map(|num| num.parse::<i32>().unwrap())
                .collect();
            board.push(line);
        }
        boards.push(board);
    }

    let (winner, last_input) = 'game: loop {
        let input = inputs.next().unwrap();
        let mut to_check = vec![];
        for (b, board) in boards.iter_mut().enumerate() {
            for (y, row) in board.iter_mut().enumerate() {
                for (x, cell) in row.iter_mut().enumerate() {
                    if *cell == input {
                        *cell = -1;
                        to_check.push((b, y, x));
                    }
                }
            }
        }
        for (b, y, x) in to_check {
            let complete_col = boards[b].iter().all(|row| row[x] == -1);
            let complete_row = boards[b][y].iter().all(|cell| *cell == -1);
            if complete_col || complete_row {
                break 'game (b, input);
            }
        }
    };
    let winner = &boards[winner];
    let sum: i32 = winner.iter()
        .flatten()
        .filter(|num| **num >= 0)
        .sum();
    let result = last_input * sum;

    println!("{}", result);
}
