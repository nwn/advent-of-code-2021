use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();

    let lines = stdin.lock().lines().map(Result::unwrap);
    let grid: Vec<Vec<_>> = lines.map(|line|
        line.bytes().map(|b| b - b'0').collect()
    ).collect();

    let mut sum = 0;
    for (y, row) in grid.iter().enumerate() {
        for (x, &cell) in row.iter().enumerate() {
            if x > 0 && cell >= grid[y][x - 1] { continue }
            if y > 0 && cell >= grid[y - 1][x] { continue }
            if x + 1 < row.len() && cell >= grid[y][x + 1] { continue }
            if y + 1 < grid.len() && cell >= grid[y + 1][x] { continue }
            sum += 1 + cell as u32;
        }
    }

    println!("{}", sum);
}
