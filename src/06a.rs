use std::io::{self, prelude::*};

fn main() {
    let stdin = io::stdin();

    let line = stdin.lock().lines().map(Result::unwrap).next().unwrap();
    let fish: Vec<_> = line.split(',').map(|num| num.parse::<usize>().unwrap()).collect();

    let mut num_with_timer = vec![0u64; 9];
    for fish in fish {
        num_with_timer[fish] += 1;
    };
    for _ in 0..80 {
        let mut updated = vec![0u64; 9];
        for x in 0..8 {
            updated[x] = num_with_timer[x + 1];
        }
        updated[6] += num_with_timer[0];
        updated[8] = num_with_timer[0];
        num_with_timer = updated;
    }
    let sum: u64 = num_with_timer.into_iter().sum();
    println!("{}", sum);
}
