## Advent of Code 2021

This repo contains my solutions to the
[2021 Advent of Code](https://adventofcode.com/2021) problem set.

The solutions are written in Rust. Each day has two puzzles, the solutions to
which are located at `src/XXa.rs` and `src/XXb.rs`, respectively, and invoked
with `cargo run --bin XXa` or `cargo run --bin XXb`. Puzzle input is read from
stdin, with my account-specific input located in the `input` directory.
